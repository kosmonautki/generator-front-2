import { Component, OnInit } from '@angular/core';
import {MemberService} from '../shared/Service/member.service';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {Member} from '../shared/model/member';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.css']
})
export class GroupsComponent implements OnInit {
  groups = new Map<Number, Member[]>();

  constructor(private memberService: MemberService, private location: Location, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {


  }

  randomGroup() {
    this.memberService.randomGroup().subscribe(value => this.groups = value)
  }
}
