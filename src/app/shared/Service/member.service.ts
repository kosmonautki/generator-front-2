import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Member} from '../model/member';


@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(private http: HttpClient) { }

  getAllMembers(): Observable<Member[]>{
    return  this.http.get<Member[]>('http://localhost:8086/rest/member/all')
  }

  save(member: Member){
    if(member.id == null){
      this.http.post('http://localhost:8086/rest/member/save', member).subscribe(value => { console.log(value);
      window.location.href = '/member';
    });
    }else {
      this.http.put('http://localhost:8086/rest/member/update', member).subscribe(value => {
        console.log(value);
        window.location.href = '/member';
      })
    }
  }


  randomGroup(): Observable<Map<Number,Member[]>>{
    return this.http.get<Map<Number,Member[]>>('http://localhost:8086/rest/member/group')
  }
}
